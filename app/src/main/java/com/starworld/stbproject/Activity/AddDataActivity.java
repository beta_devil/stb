package com.starworld.stbproject.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.starworld.stbproject.Adaptors.ActiveConnectionAdaptor;
import com.starworld.stbproject.Adaptors.AllPlanDataAdaptor;
import com.starworld.stbproject.Items.ActiveConnectionItem;
import com.starworld.stbproject.Items.AllPlanItems;
import com.starworld.stbproject.Items.UserInfo;
import com.starworld.stbproject.R;
import com.starworld.stbproject.Utils.Constant;
import com.starworld.stbproject.Utils.DroidPrefs;
import com.starworld.stbproject.Volley.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddDataActivity extends AppCompatActivity {

    @BindView(R.id.toolbarAdddata)
    Toolbar toolbar;
    @BindView(R.id.recycle_all_data)
    RecyclerView recycle_all_data;
    @BindView(R.id.no_result_txt)
    TextView no_result_txt;
    private LinearLayoutManager linearLayoutManager;
    List<AllPlanItems> allPlanItemsList;
    AllPlanDataAdaptor allPlanDataAdaptor;
    private KProgressHUD kProgressHUD;
    UserInfo userInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Add Data");

        userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);

        linearLayoutManager=new LinearLayoutManager(this);
        recycle_all_data.setLayoutManager(linearLayoutManager);
        getAllPlan();
    }

    public void getAllPlan(){
        String url= "http://mydigisolution.com/partner/stvjson/allplans.php?api_key="+userInfo.api_key+"&dis_key="+userInfo.dis_key;
        kProgressHUD= Constant.showDialog(this);

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    kProgressHUD.dismiss();
                    try {
                        JSONObject jsonObject=new JSONObject(response);
                        UserInfo userInfo=null;
                        allPlanItemsList=new ArrayList<>();
                        if (jsonObject.getString("responseCode").equals("1")){

                            Toast.makeText(AddDataActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();
                            JSONArray jsonArray = jsonObject.getJSONArray("responseUserinfo");


                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                AllPlanItems allPlanItems=new AllPlanItems();
                                allPlanItems.package_name=jsonObject1.getString("package_name");
                                allPlanItems.duration=jsonObject1.getString("duration");
                                allPlanItems.Fee=jsonObject1.getString("Fee");
                                allPlanItems.details=jsonObject1.getString("details");

                                allPlanItemsList.add(allPlanItems);
                            }

                        }
                        if (jsonObject.getString("responseCode").equals("0")){
                            Toast.makeText(AddDataActivity.this,jsonObject.getString("responseMessage") , Toast.LENGTH_SHORT).show();

                        }

                        if (allPlanItemsList.size()>0)
                        {
                            recycle_all_data.setVisibility(View.VISIBLE);
                            no_result_txt.setVisibility(View.GONE);
                            setAdaptor();
                        }
                        else{
                            recycle_all_data.setVisibility(View.GONE);
                            no_result_txt.setVisibility(View.VISIBLE);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                kProgressHUD.dismiss();
                String message= Constant.checkConnectionError(error);
                Toast.makeText(AddDataActivity.this, message, Toast.LENGTH_SHORT).show();

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));


    }

    public void setAdaptor(){
        allPlanDataAdaptor=new AllPlanDataAdaptor(this, allPlanItemsList, new AllPlanDataAdaptor.Clicked() {
            @Override
            public void clicked(int pos, View view) {

            }
        });
        recycle_all_data.setAdapter(allPlanDataAdaptor);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                super. onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
