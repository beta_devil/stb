package com.starworld.stbproject.Activity;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.starworld.stbproject.Items.UserInfo;
import com.starworld.stbproject.R;
import com.starworld.stbproject.Utils.Constant;
import com.starworld.stbproject.Utils.DroidPrefs;
import com.starworld.stbproject.Volley.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangePassword extends AppCompatActivity {

    @BindView(R.id.old_pass)
    EditText old_pass;
    @BindView(R.id.new_pass)
    EditText new_pass;
    @BindView(R.id.new_confirm_pass)
    EditText new_confirm_pass;
    @BindView(R.id.submit)
    TextView submit;
    @BindView(R.id.toolbarprofile)
    Toolbar toolbar;
    private KProgressHUD kProgressHUD;
    UserInfo userInfo;
    String oldpass,newPass,newConfirmPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Change Password");
        userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldpass=old_pass.getText().toString();
                newPass=new_pass.getText().toString();
                newConfirmPass=new_confirm_pass.getText().toString();

                if (oldpass.equals("") || newPass.equals("") || newConfirmPass.equals("")){
                    Toast.makeText(ChangePassword.this, "All feilds are mandetory", Toast.LENGTH_SHORT).show();
                 }
                else if (!newPass.equals(newConfirmPass)){
                    Toast.makeText(ChangePassword.this, "New password and confirm password not match", Toast.LENGTH_SHORT).show();

                }
                else if (newPass.equals(oldpass)){
                    Toast.makeText(ChangePassword.this, "Old password and new password are same", Toast.LENGTH_SHORT).show();
                }
                else{

                    if (Constant.isInternetConnected(ChangePassword.this))
                    {
                        changePass();
                    }
                    else{
                        Toast.makeText(ChangePassword.this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }

                }



            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                super. onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void changePass(){
        String url= "http://mydigisolution.com/partner/stvjson/cb_alldetails.php?api_key="+userInfo.api_key+"&dis_key="+userInfo.dis_key;
        kProgressHUD=Constant.showDialog(this);

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    kProgressHUD.dismiss();
                    try {
                        JSONObject jsonObject=new JSONObject(response);
                        if (jsonObject.getString("error").equals("1")) {
                            Toast.makeText(ChangePassword.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();


                        }
                        else if (jsonObject.getString("error").equals("0")){
                            Toast.makeText(ChangePassword.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                kProgressHUD.dismiss();
                String message= Constant.checkConnectionError(error);
                Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));



    }
}
