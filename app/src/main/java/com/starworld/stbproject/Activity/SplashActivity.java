package com.starworld.stbproject.Activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.starworld.stbproject.Items.UserInfo;
import com.starworld.stbproject.MainActivity;
import com.starworld.stbproject.R;
import com.starworld.stbproject.Utils.DroidPrefs;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2500;
    private UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splesh);

        userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);
        new Handler().postDelayed(new Runnable() {

            public void run() {

                if (userInfo!=null && userInfo.api_key!=null){
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                }
                else {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                }
                //code for animation........
                // overridePendingTransition(R.anim.right, R.anim.left);

                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
