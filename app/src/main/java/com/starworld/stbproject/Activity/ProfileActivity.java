package com.starworld.stbproject.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.starworld.stbproject.Items.UserInfo;
import com.starworld.stbproject.MainActivity;
import com.starworld.stbproject.R;
import com.starworld.stbproject.Utils.Constant;
import com.starworld.stbproject.Utils.DroidPrefs;
import com.starworld.stbproject.Volley.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.edt_fullname)
    EditText FullName;
    @BindView(R.id.edt_contect)
    EditText Contact;
    @BindView(R.id.edt_Emialid)
    EditText EmailId;
   /* @BindView(R.id.edt_fullname)
    EditText EmployeId;*/
    @BindView(R.id.edt_address)
    EditText Address;
    @BindView(R.id.edt_city)
    EditText AreaName;
    @BindView(R.id.btn_Update)
    TextView Update;
    @BindView(R.id.btn_change_pass)
    TextView change_pass;
    @BindView(R.id.toolbarprofile)
    Toolbar toolbar;
    @BindView(R.id.userImg)
    ImageView img_userpic;
    KProgressHUD kProgressHUD;
    private UserInfo userInfo;

    boolean camera_denied,gallery_denied;
    private static final int PIC_CROP = 3;
    private Uri picUri;
    private String profile_image="";
    private static final int CAMERA_REQUEST = 1888;
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = { Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    Bitmap default_bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Profile");

        userInfo=DroidPrefs.get(this,"user_info",UserInfo.class);
        getProfile();

        Update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FullName.getText().toString().equals("") || EmailId.getText().toString().equals("") ||
                         Address.getText().toString().equals(""))
                {
                    Toast.makeText(ProfileActivity.this, "All Fields are mandetory", Toast.LENGTH_SHORT).show();
                }
                else if(emailValidator(EmailId.getText().toString().trim())==false){
                    Toast.makeText(ProfileActivity.this, "Enter valid E-mail ID", Toast.LENGTH_SHORT).show();
                }
                else {
                    updateProfile();
                }
            }
        });

        change_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ProfileActivity.this,ChangePassword.class);
                startActivity(i);
            }
        });

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }



        img_userpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogForPicture();
            }
        });


    }
    private void showDialogForPicture() {
        final Dialog dialog=new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogThemeMain;
        dialog.setContentView(R.layout.dialog_layout);

        TextView title=dialog.findViewById(R.id.title);
        TextView message=dialog.findViewById(R.id.message);
        TextView second_message=dialog.findViewById(R.id.second_message);
        TextView ok=dialog.findViewById(R.id.ok);
        TextView cancel=dialog.findViewById(R.id.cancel);
        title.setText("Select Action");
        message.setText("Capture photo from camera");
        second_message.setText("Select photo from gallery");

        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if(!camera_denied) {
                    dialog.dismiss();
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

//folder stuff
                    File imagesFolder = new File(Environment.getExternalStorageDirectory(), "MyBus");
                    imagesFolder.mkdirs();

                    File image = new File(imagesFolder, "profile_admin.png");
                    if (Build.VERSION.SDK_INT >= 24) {
                        picUri = FileProvider.getUriForFile(ProfileActivity.this, getPackageName() + ".provider", image);
                    } else {
                        picUri = Uri.fromFile(image);
                    }
//

                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
                else {
                    //showalert_camera();
                    Toast.makeText(ProfileActivity.this, "Please allow Permission", Toast.LENGTH_SHORT).show();
                }


            }
        });
        second_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!gallery_denied) {
                    dialog.dismiss();
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, 2);
                }
                else{
                    //showalert_camera();
                    Toast.makeText(ProfileActivity.this, "Please allow Permission", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });



    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                super. onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getProfile(){
        String url= "http://mydigisolution.com/partner/stvjson/cb_alldetails.php?api_key="+userInfo.api_key+"&dis_key="+userInfo.dis_key;
        kProgressHUD=Constant.showDialog(this);

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    kProgressHUD.dismiss();
                    try {
                        JSONObject jsonObject=new JSONObject(response);
                        if (jsonObject.getString("error").equals("1")) {
                            Toast.makeText(ProfileActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();
                            JSONArray jsonArray=new JSONArray(jsonObject.getString("user"));

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                userInfo.dis_mailid=jsonObject1.getString("dis_mailid");
                                userInfo.cb_name=jsonObject1.getString("cb_name");
                                userInfo.cb_mobile=jsonObject1.getString("cb_mobile");
                                userInfo.cb_mailid=jsonObject1.getString("cb_mailid");
                                userInfo.area_name=jsonObject1.getString("area_name");
                                userInfo.address=jsonObject1.getString("address");
                               // userInfo.image=jsonObject1.getString("address");
                            }

                            setData(userInfo);
                            DroidPrefs.apply(ProfileActivity.this,"user_info",userInfo);

                        }
                        else if (jsonObject.getString("error").equals("0")){
                            Toast.makeText(ProfileActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                kProgressHUD.dismiss();
                String message= Constant.checkConnectionError(error);
                Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_SHORT).show();

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));



    }

    public void updateProfile(){
        String url= "http://mydigisolution.com/partner/stvjson/cb_updateprofile.php?api_key="+
                userInfo.api_key+"&dis_key="+userInfo.dis_key+"&cb_name="+FullName.getText().toString()+
                "&cb_mailid="+EmailId.getText().toString().trim()+ "&address="+Address.getText().toString();
        kProgressHUD=Constant.showDialog(this);

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    kProgressHUD.dismiss();
                    try {
                        JSONObject jsonObject=new JSONObject(response);

                        if (jsonObject.getString("responseCode").equals("1")) {
                            Toast.makeText(ProfileActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else if (jsonObject.getString("error").equals("0")){
                            Toast.makeText(ProfileActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                kProgressHUD.dismiss();
                String message= Constant.checkConnectionError(error);
                Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_SHORT).show();

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));

    }

    public void setData(UserInfo userInfo){
        FullName.setText(userInfo.cb_name);
        Contact.setText(userInfo.cb_mobile);
        EmailId.setText(userInfo.cb_mailid);
        AreaName.setText(userInfo.area_name);
        Address.setText(userInfo.address);
    }

    public boolean emailValidator(String email)
    {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(String permission: permissions){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                //denied
                Log.e("denied", permission);
                if(permission.equalsIgnoreCase("android.permission.CAMERA")){
                    camera_denied=true;
                }
                else if(permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")){
                    gallery_denied=true;
                }

            }else{
                if(ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED){
                    //allowed
                    Log.e("allowed", permission);
                    if(permission.equalsIgnoreCase("android.permission.CAMERA")){
                        camera_denied=false;
                    }
                    else if(permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")){
                        gallery_denied=false;
                    }

                } else{
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    if(permission.equalsIgnoreCase("android.permission.CAMERA")){
                        camera_denied=true;
                    }
                    else if(permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")){
                        gallery_denied=true;
                    }
                    //do something here.
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
//
            Intent intent = new Intent(ProfileActivity.this, CroppingActivity.class);
            intent.putExtra("uri", picUri.toString());
            startActivityForResult(intent, PIC_CROP);
//
//


        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {

            if (data != null) {
                picUri = data.getData();
                Intent intent = new Intent(ProfileActivity.this, CroppingActivity.class);
                intent.putExtra("uri", picUri.toString());
                startActivityForResult(intent, PIC_CROP);
//
//
            }

        } else if (requestCode == PIC_CROP && resultCode == Activity.RESULT_OK) {
            if (data != null) {

                String path = data.getStringExtra("image");
                // get the returned data

                default_bitmap = BitmapFactory.decodeFile(path);
                if (default_bitmap!=null){
                    profile_image=getimage(default_bitmap);

                   /* ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    default_bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Glide.with(this).asBitmap()
                            .load(stream.toByteArray())
                            .apply(bitmapTransform(new CropCircleTransformation()))
                            .into(userImage);*/

                   /* Glide.with(this).load(default_bitmap)
                            .apply(bitmapTransform(new CropCircleTransformation()))
                            .into((userImage));*/
                    //userImage.setImageBitmap(default_bitmap);
                    //user_img.setBorderWidth(0);
                }
               /* if (default_bitmap != null) {
                    int width = default_bitmap.getWidth();
                    int height = default_bitmap.getHeight();
                    if (width > 1000 & height > 1200) {
                        default_bitmap = Bitmap.createScaledBitmap(default_bitmap, 700, 700, false);




                    }


                }
*/
            }}

        else if(requestCode==0){
            for(String permission: PERMISSIONS){
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                    //denied
                    Log.e("denied", permission);
                    if(permission.equalsIgnoreCase("android.permission.CAMERA")){
                        camera_denied=true;
                    }
                    else if(permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")){
                        gallery_denied=true;
                    }

                }else{
                    if(ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED){
                        //allowed
                        Log.e("allowed", permission);
                        if(permission.equalsIgnoreCase("android.permission.CAMERA")){
                            camera_denied=false;
                        }
                        else if(permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")){
                            gallery_denied=false;
                        }

                    } else{
                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        if(permission.equalsIgnoreCase("android.permission.CAMERA")){
                            camera_denied=true;
                        }
                        else if(permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")){
                            gallery_denied=true;
                        }
                        //do something here.
                    }
                }
            }

        }
    }

    public String getimage(Bitmap bitmap) {


        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        bitmap =bitmap.createScaledBitmap(bitmap,100,100,false);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArray);
        byte[] byteArr = byteArray.toByteArray();
        String base64 = Base64.encodeToString(byteArr, Base64.NO_WRAP);

        return base64;

    }
}
