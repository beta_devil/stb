package com.starworld.stbproject.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.kaopiz.kprogresshud.KProgressHUD;

import com.starworld.stbproject.R;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by yugasalabs-16 on 27/3/17.
 */

public class CroppingActivity extends AppCompatActivity {
    @BindView(R.id.cropImageView)
    CropImageView cropImageView;
    Bitmap bitmap;
    @BindView(R.id.crop)
    ImageView crop;
    Intent intent;


    ActionBar actionBar;

    Uri uri;
    @BindView(R.id.img_rotate)
    ImageView img_rotate;
    int rotation;
    Handler handler;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_cropping);
        ButterKnife.bind(this);

        intent=getIntent();
        uri= Uri.parse(intent.getStringExtra("uri"));
        cropImageView.setImageUriAsync(uri);
        cropImageView.setFixedAspectRatio(true);
        cropImageView.setAutoZoomEnabled(false);
        cropImageView.setAspectRatio(7,7);
        crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //cropImageView.getRotation();
               // Rect rect = cropImageView.getCropRect();
                // rotation = cropImageView.getRotatedDegrees();

                handler=new Handler();
                final KProgressHUD kProgressHUD ;
                kProgressHUD= KProgressHUD.create(CroppingActivity.this);
                kProgressHUD .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
                kProgressHUD  .setCancellable(false);
                kProgressHUD .setAnimationSpeed(2);
                kProgressHUD .setDimAmount(0.5f);
                kProgressHUD .show();
               handler.postDelayed(new Runnable() {

    @Override
    public void run() {
                   bitmap=cropImageView.getCroppedImage();

                    kProgressHUD.dismiss();
                    savebitmap(bitmap);
                }
             },500);

//
            }
        });
        img_rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cropImageView.rotateImage((int) (cropImageView.getRotation()+90));


            }
        });
    }
    public byte[] getimage(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
//        String base64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
//        return base64;
        return byteArray;
    };
    private void savebitmap(Bitmap bitmap) {
        File file = null;
        Random random=new Random();
        int rand_name=random.nextInt();
        try {
            String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                    "/MySTb";
            File dir = new File(file_path);
            if (!dir.exists())
                dir.mkdirs();
          //  file = new File(dir, "cropped_image/*"+rand_name+ "*/.png");
            file = new File(dir, "profile_admin.png");
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap =bitmap.createScaledBitmap(bitmap,700,700,false);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
            Intent intent =new Intent();
            intent.putExtra("image",file.getPath());
            setResult(Activity.RESULT_OK,intent);
            finish();


        }
        catch(Exception e){

        }


    }
    public class GetImage extends AsyncTask<String,Integer,Bitmap> {
        KProgressHUD kProgressHUD ;
        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bitmap;
            bitmap=cropImageView.getCroppedImage();



            return bitmap;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            kProgressHUD.dismiss();
            super.onPostExecute(bitmap);
            if(bitmap!=null){
                savebitmap(bitmap);
            }
        }
    }


}
