package com.starworld.stbproject.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.starworld.stbproject.Adaptors.ActiveConnectionAdaptor;
import com.starworld.stbproject.Items.ActiveConnectionItem;
import com.starworld.stbproject.Items.UserInfo;
import com.starworld.stbproject.MainActivity;
import com.starworld.stbproject.R;
import com.starworld.stbproject.Utils.Constant;
import com.starworld.stbproject.Utils.DroidPrefs;
import com.starworld.stbproject.Volley.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActiveConectionActivity extends AppCompatActivity {


     @BindView(R.id.toolbarActivec)
     Toolbar toolbarActive;
    @BindView(R.id.recycle_active_connection)
    RecyclerView recycle_active_connection;
    @BindView(R.id.no_result_txt)
    TextView no_result_txt;
    private LinearLayoutManager linearLayoutManager;
    List<ActiveConnectionItem> activeConnectionItemList;
    ActiveConnectionAdaptor activeConnectionAdaptor;
    private KProgressHUD kProgressHUD;
    UserInfo userInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_conection);
        ButterKnife.bind(this);

        setSupportActionBar(toolbarActive);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Active Connection");

        userInfo=DroidPrefs.get(this,"user_info",UserInfo.class);

        linearLayoutManager=new LinearLayoutManager(this);
        recycle_active_connection.setLayoutManager(linearLayoutManager);
         getActiveConnection();


    }



    public void getActiveConnection(){
        String url= "http://mydigisolution.com/partner/api/active_connection.php?stv_key="+userInfo.api_key;
        kProgressHUD= Constant.showDialog(this);

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    kProgressHUD.dismiss();
                    try {
                        JSONObject jsonObject=new JSONObject(response);
                        UserInfo userInfo=null;
                        activeConnectionItemList=new ArrayList<>();
                        if (jsonObject.getString("code").equals("1")){

                            Toast.makeText(ActiveConectionActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();
                            JSONArray jsonArray = jsonObject.getJSONArray("user");


                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                ActiveConnectionItem activeConnectionItem=new ActiveConnectionItem();
                                activeConnectionItem.customer_name=jsonObject1.getString("customer");
                                activeConnectionItem.customer_user_key=jsonObject1.getString("user_key");
                                activeConnectionItem.customer_id=jsonObject1.getString("id");
                                activeConnectionItem.mobileno=jsonObject1.getString("mobile");
                                activeConnectionItem.connectiono=jsonObject1.getString("connection");
                                activeConnectionItem.city=jsonObject1.getString("city");
                                activeConnectionItem.address=jsonObject1.getString("address");
                                 activeConnectionItemList.add(activeConnectionItem);
                            }

                        }
                       else if (jsonObject.getString("responseCode").equals("0")){
                            Toast.makeText(ActiveConectionActivity.this,jsonObject.getString("responseMessage") , Toast.LENGTH_SHORT).show();

                        }

                        if (activeConnectionItemList.size()>0)
                        {
                            recycle_active_connection.setVisibility(View.VISIBLE);
                            no_result_txt.setVisibility(View.GONE);
                            setAdaptor();
                        }
                        else{
                            recycle_active_connection.setVisibility(View.GONE);
                            no_result_txt.setVisibility(View.VISIBLE);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                kProgressHUD.dismiss();
                String message= Constant.checkConnectionError(error);
                Toast.makeText(ActiveConectionActivity.this, message, Toast.LENGTH_SHORT).show();

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));


    }

    public void setAdaptor(){
        activeConnectionAdaptor=new ActiveConnectionAdaptor(this, activeConnectionItemList, new ActiveConnectionAdaptor.Clicked() {
            @Override
            public void clicked(int pos, View view) {
                  deactiveConnection(pos);
            }
        });
        recycle_active_connection.setAdapter(activeConnectionAdaptor);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                super. onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void deactiveConnection(int pos){
        String url= "http://mydigisolution.com/partner/api/active_connection_post.php?stv_key="+userInfo.api_key+"&userkey="+activeConnectionItemList.get(pos).customer_user_key;
        kProgressHUD= Constant.showDialog(this);

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    kProgressHUD.dismiss();
                    try {
                        JSONObject jsonObject=new JSONObject(response);
                        UserInfo userInfo=null;
                        activeConnectionItemList=new ArrayList<>();
                        if (jsonObject.getString("code").equals("1")){

                            Toast.makeText(ActiveConectionActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();


                        }
                        else if (jsonObject.getString("code").equals("0")){
                            Toast.makeText(ActiveConectionActivity.this,jsonObject.getString("responseMessage") , Toast.LENGTH_SHORT).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                kProgressHUD.dismiss();
                String message= Constant.checkConnectionError(error);
                Toast.makeText(ActiveConectionActivity.this, message, Toast.LENGTH_SHORT).show();

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));


    }
}
