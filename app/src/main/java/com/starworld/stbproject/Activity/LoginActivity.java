package com.starworld.stbproject.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.starworld.stbproject.Items.UserInfo;
import com.starworld.stbproject.MainActivity;
import com.starworld.stbproject.R;
import com.starworld.stbproject.Utils.Constant;
import com.starworld.stbproject.Utils.DroidPrefs;
import com.starworld.stbproject.Volley.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.btn_loginSubmit)
    Button loginSubmit;
    public SharedPreferences.Editor loginPrefsEditor;
    public SharedPreferences loginPreferences;
    private Boolean saveLogin;

    TextView registerr,forgetpass;
    KProgressHUD kProgressHUD;
    @BindView(R.id.edt_userid)
    EditText UserId;
    @BindView(R.id.edt_password)
    EditText password;

    String UserId1,pass1;
    @BindView(R.id.txt_ForgetPassword)
    TextView ForgetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        ForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);
            }
        });

        loginSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserId1 = UserId.getText().toString();
                pass1 = password.getText().toString();
                if (UserId1.equals("") || pass1.equals("")){
                    Toast.makeText(LoginActivity.this, "Please enter username and password", Toast.LENGTH_SHORT).show();
                }
                else{
                    loginData(UserId1,pass1);
                }

            }
        });
    }



    public void loginData(String userId,String pass){
        String url= "http://mydigisolution.com/partner/stvjson/ds_login.php?cb_mobile="+userId+"&cb_pwd="+pass;
        kProgressHUD=Constant.showDialog(this);

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    kProgressHUD.dismiss();
                    try {
                        JSONObject jsonObject=new JSONObject(response);
                        UserInfo userInfo=null;
                        if (jsonObject.getString("error").equals("1")){

                            Toast.makeText(LoginActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();
                            JSONArray jsonArray=new JSONArray(jsonObject.getString("user"));

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                 userInfo=new UserInfo();
                                userInfo.api_key=jsonObject1.getString("api_key");
                                userInfo.dis_key=jsonObject1.getString("dis_key");
                                userInfo.dis_mailid=jsonObject1.getString("dis_mailid");
                                userInfo.cb_name=jsonObject1.getString("cb_name");
                                userInfo.cb_mobile=jsonObject1.getString("cb_mobile");
                            }
                            DroidPrefs.apply(LoginActivity.this,"user_info",userInfo);
                            Intent i=new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(i);
                            finish();
                        }
                        if (jsonObject.getString("error").equals("0")){
                            Toast.makeText(LoginActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                kProgressHUD.dismiss();
                String message= Constant.checkConnectionError(error);
                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));


    }


}
