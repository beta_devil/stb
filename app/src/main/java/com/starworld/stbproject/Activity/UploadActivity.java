package com.starworld.stbproject.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.starworld.stbproject.Items.UserInfo;
import com.starworld.stbproject.R;
import com.starworld.stbproject.Utils.Constant;
import com.starworld.stbproject.Utils.DroidPrefs;
import com.starworld.stbproject.Volley.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UploadActivity extends AppCompatActivity {


    @BindView(R.id.toolbarprofileinfo)
    Toolbar toolbar;
    @BindView(R.id.edt_customername)
    EditText CustomerName;
    @BindView(R.id.edt_mailid)
    EditText EmailId;
    @BindView(R.id.edt_mobieno)
    EditText MobileNo;
    @BindView(R.id.edt_address)
    EditText Address;
    @BindView(R.id.edt_connection)
    EditText Connectionno;
    @BindView(R.id.edt_city)
    EditText City;
    @BindView(R.id.edt_password)
    EditText Password;
    @BindView(R.id.edt_date)
    TextView date;
    @BindView(R.id.edt_plan)
    EditText Plan;
    @BindView(R.id.edt_Block)
    EditText Block;
    @BindView(R.id.edt_vconenumber)
    EditText VCOneNumber;
    @BindView(R.id.edt_vctwonumber)
    EditText VCTwoNumber;
    @BindView(R.id.edt_vcthreenumber)
    EditText VCThreeNumber;
    @BindView(R.id.edt_stvonenumber)
    EditText STVOneNumber;
    @BindView(R.id.edt_stvtwonumber)
    EditText STVTwoNumber;
    @BindView(R.id.edt_stvthreenumber)
    EditText STVThreeNumber;
    @BindView(R.id.userImg)
    ImageView img_userpic;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.btn_next)
    Button btn_next;


    UserInfo userInfo;
    String  sCustomerName,sEmailId,sMobileNo,sAreaName,sAddress,sConnectionno,sCity,sPassword,sDate,
            sPlan,sBlock,sVCOneNumber,sVCTwoNumber,sVCThreeNumber,sSTVOneNumber,sSTVTwoNumber,sSTVThreeNumber;
    private int mYear, mMonth, mDay, mHour, mMinute;
    String   ImageName="file";
    String   customer1="customer";
    String   MobileNo1="mobile";
    String   EmailId1="email";
    String   Address1="address";
    String   Connectionno1="connection";
    String   City1="city";
    String   Password1="password";
    String   Plan1="plan";
    String   Block1="block";
    String   Date1="date";
    String   VCOneNumber1="vc_number_1";
    String   VCTwoNumber1="vc_number_2";
    String   VCThreeNumber1="vc_number_3";
    String   STVOneNumber1="stv_number_1";
    String   STVTwoNumber1="stv_number_2";
    String   STVThreeNumber1="stv_number_3";
    String    ServerUploadPath ="http://mydigisolution.com/partner/api/new_customer_reg.php";


    boolean camera_denied,gallery_denied;
    private static final int PIC_CROP = 3;
    private Uri picUri;
    private String profile_image="";
    private static final int CAMERA_REQUEST = 1888;
    int PERMISSION_ALL = 1;
    KProgressHUD kProgressHUD;
    String[] PERMISSIONS = { Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    Bitmap default_bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Customer Info");

        userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
        img_userpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogForPicture();
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(UploadActivity.this,new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sCustomerName=CustomerName.getText().toString();
                sEmailId=EmailId.getText().toString();
                sMobileNo=MobileNo.getText().toString();
                sDate=date.getText().toString();
                sAddress=Address.getText().toString();
                sCity=City.getText().toString();
                sPassword=Password.getText().toString();
                sPlan=Plan.getText().toString();
                sBlock=Block.getText().toString();
                sVCOneNumber=VCOneNumber.getText().toString();
                sVCTwoNumber=VCTwoNumber.getText().toString();
                sVCThreeNumber=VCThreeNumber.getText().toString();
                sSTVOneNumber=STVOneNumber.getText().toString();
                sSTVTwoNumber=STVTwoNumber.getText().toString();
                sSTVThreeNumber=STVThreeNumber.getText().toString();
                sConnectionno=Connectionno.getText().toString();
                if (sCustomerName.equals("")){
                    Toast.makeText(UploadActivity.this, "Please enter Customer name", Toast.LENGTH_SHORT).show();
                }
                else if(sEmailId.equals("")){
                    Toast.makeText(UploadActivity.this, "Please enter Email-Id", Toast.LENGTH_SHORT).show();

                }
                else if(sMobileNo.equals("")){
                    Toast.makeText(UploadActivity.this, "Please enter Mobile No.", Toast.LENGTH_SHORT).show();

                } else if(emailValidator(sEmailId)==false){
                    Toast.makeText(UploadActivity.this, "Please enter valid Email-id", Toast.LENGTH_SHORT).show();

                } else if(sMobileNo.length()!=10){
                    Toast.makeText(UploadActivity.this, "Please enter 10 Digit Mobile number", Toast.LENGTH_SHORT).show();

                }
                else{
                    updateCustomerInfo();
                }

            }
        });



    }

    public boolean emailValidator(String email)
    {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private void showDialogForPicture() {
        final Dialog dialog=new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogThemeMain;
        dialog.setContentView(R.layout.dialog_layout);

        TextView title=dialog.findViewById(R.id.title);
        TextView message=dialog.findViewById(R.id.message);
        TextView second_message=dialog.findViewById(R.id.second_message);
        TextView ok=dialog.findViewById(R.id.ok);
        TextView cancel=dialog.findViewById(R.id.cancel);
        title.setText("Select Action");
        message.setText("Capture photo from camera");
        second_message.setText("Select photo from gallery");

        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if(!camera_denied) {
                    dialog.dismiss();
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

//folder stuff
                    File imagesFolder = new File(Environment.getExternalStorageDirectory(), "MyBus");
                    imagesFolder.mkdirs();

                    File image = new File(imagesFolder, "profile_admin.png");
                    if (Build.VERSION.SDK_INT >= 24) {
                        picUri = FileProvider.getUriForFile(UploadActivity.this, getPackageName() + ".provider", image);
                    } else {
                        picUri = Uri.fromFile(image);
                    }
//

                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
                else {
                    //showalert_camera();
                    Toast.makeText(UploadActivity.this, "Please allow Permission", Toast.LENGTH_SHORT).show();
                }


            }
        });
        second_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!gallery_denied) {
                    dialog.dismiss();
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, 2);
                }
                else{
                    //showalert_camera();
                    Toast.makeText(UploadActivity.this, "Please allow Permission", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

       dialog.show();

    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for(String permission: permissions){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                //denied
                Log.e("denied", permission);
                if(permission.equalsIgnoreCase("android.permission.CAMERA")){
                    camera_denied=true;
                }
                else if(permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")){
                    gallery_denied=true;
                }

            }else{
                if(ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED){
                    //allowed
                    Log.e("allowed", permission);
                    if(permission.equalsIgnoreCase("android.permission.CAMERA")){
                        camera_denied=false;
                    }
                    else if(permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")){
                        gallery_denied=false;
                    }

                } else{
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    if(permission.equalsIgnoreCase("android.permission.CAMERA")){
                        camera_denied=true;
                    }
                    else if(permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")){
                        gallery_denied=true;
                    }
                    //do something here.
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
//
            Intent intent = new Intent(UploadActivity.this, CroppingActivity.class);
            intent.putExtra("uri", picUri.toString());
            startActivityForResult(intent, PIC_CROP);
//
//


        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {

            if (data != null) {
                picUri = data.getData();
                Intent intent = new Intent(UploadActivity.this, CroppingActivity.class);
                intent.putExtra("uri", picUri.toString());
                startActivityForResult(intent, PIC_CROP);
//
//
            }

        } else if (requestCode == PIC_CROP && resultCode == Activity.RESULT_OK) {
            if (data != null) {

                String path = data.getStringExtra("image");
                // get the returned data

                default_bitmap = BitmapFactory.decodeFile(path);
                if (default_bitmap!=null){
                    profile_image=getimage(default_bitmap);
                    img_userpic.setImageBitmap(default_bitmap);


                   /* ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    default_bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Glide.with(this).asBitmap()
                            .load(stream.toByteArray())
                            .apply(bitmapTransform(new CropCircleTransformation()))
                            .into(userImage);*/

                   /* Glide.with(this).load(default_bitmap)
                            .apply(bitmapTransform(new CropCircleTransformation()))
                            .into((userImage));*/
                    //userImage.setImageBitmap(default_bitmap);
                    //user_img.setBorderWidth(0);
                }
               /* if (default_bitmap != null) {
                    int width = default_bitmap.getWidth();
                    int height = default_bitmap.getHeight();
                    if (width > 1000 & height > 1200) {
                        default_bitmap = Bitmap.createScaledBitmap(default_bitmap, 700, 700, false);




                    }


                }
*/
            }}

        else if(requestCode==0){
            for(String permission: PERMISSIONS){
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                    //denied
                    Log.e("denied", permission);
                    if(permission.equalsIgnoreCase("android.permission.CAMERA")){
                        camera_denied=true;
                    }
                    else if(permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")){
                        gallery_denied=true;
                    }

                }else{
                    if(ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED){
                        //allowed
                        Log.e("allowed", permission);
                        if(permission.equalsIgnoreCase("android.permission.CAMERA")){
                            camera_denied=false;
                        }
                        else if(permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")){
                            gallery_denied=false;
                        }

                    } else{
                        //set to never ask again
                        Log.e("set to never ask again", permission);
                        if(permission.equalsIgnoreCase("android.permission.CAMERA")){
                            camera_denied=true;
                        }
                        else if(permission.equalsIgnoreCase("android.permission.READ_EXTERNAL_STORAGE") || permission.equalsIgnoreCase("android.permission.WRITE_EXTERNAL_STORAGE")){
                            gallery_denied=true;
                        }
                        //do something here.
                    }
                }
            }

        }
    }

    public String getimage(Bitmap bitmap) {


        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        bitmap =bitmap.createScaledBitmap(bitmap,100,100,false);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArray);
        byte[] byteArr = byteArray.toByteArray();
        String base64 = Base64.encodeToString(byteArr, Base64.NO_WRAP);

        return base64;

    }

    public void updateCustomerInfo(){

        String url="http://mydigisolution.com/partner/api/new_customer_reg.php?api_key="+userInfo
                .api_key+"&";
        kProgressHUD= Constant.showDialog(this);

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    kProgressHUD.dismiss();
                    try {
                        JSONObject jsonObject=new JSONObject(response);

                        if (jsonObject.getString("responseCode").equals("1")) {
                            Toast.makeText(UploadActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else if (jsonObject.getString("error").equals("0")){
                            Toast.makeText(UploadActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                kProgressHUD.dismiss();
                String message= Constant.checkConnectionError(error);
                Toast.makeText(UploadActivity.this, message, Toast.LENGTH_SHORT).show();

            }
        });/*{
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> HashMapParams = new HashMap<String, String>();
                HashMapParams.put(ImageName, profile_image);
                HashMapParams.put(Date1, sDate);
                HashMapParams.put(customer1, sCustomerName);
                HashMapParams.put(EmailId1, sEmailId);
                HashMapParams.put(MobileNo1,sMobileNo);
                HashMapParams.put(Address1, sAddress);
                HashMapParams.put(Connectionno1, sConnectionno);
                HashMapParams.put(City1, sCity);
                HashMapParams.put(Password1, sPassword);
                HashMapParams.put(Plan1, sPlan);
                HashMapParams.put(Block1, sBlock);
                HashMapParams.put(VCOneNumber1, sVCOneNumber);
                HashMapParams.put(VCTwoNumber1, sVCTwoNumber);
                HashMapParams.put(VCThreeNumber1, sVCThreeNumber);
                HashMapParams.put(STVOneNumber1, sSTVOneNumber);
                HashMapParams.put(STVTwoNumber1, sSTVTwoNumber);
                HashMapParams.put(STVThreeNumber1, sSTVThreeNumber);

                return HashMapParams;
            }
        };*/
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case 16908332:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
