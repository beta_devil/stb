package com.starworld.stbproject.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.starworld.stbproject.Adaptors.ActiveConnectionAdaptor;
import com.starworld.stbproject.Adaptors.AllCustomerAdaptor;
import com.starworld.stbproject.Adaptors.AllPlanDataAdaptor;
import com.starworld.stbproject.Items.ActiveConnectionItem;
import com.starworld.stbproject.Items.AllCustomerItems;
import com.starworld.stbproject.Items.UserInfo;
import com.starworld.stbproject.R;
import com.starworld.stbproject.Utils.Constant;
import com.starworld.stbproject.Utils.DroidPrefs;
import com.starworld.stbproject.Volley.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AllCustomersActivity extends AppCompatActivity {

    @BindView(R.id.toolbarcustomer)
    Toolbar toolbar;

    @BindView(R.id.recycle_all_customer)
    RecyclerView recycle_all_customer;
    @BindView(R.id.no_result_txt)
    TextView no_result_txt;
    private LinearLayoutManager linearLayoutManager;
    List<AllCustomerItems> allCustomerItemsList;
    AllCustomerAdaptor allCustomerAdaptor;
    private KProgressHUD kProgressHUD;
    UserInfo userInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("All Customer");


        userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);

        linearLayoutManager=new LinearLayoutManager(this);
        recycle_all_customer.setLayoutManager(linearLayoutManager);
        getAllCustomer();



      /*  customerL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(AllCustomersActivity.this,CustomerDetailsActivity.class);
                startActivity(intent);
            }
        });*/


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                super. onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getAllCustomer(){
        String url= "http://mydigisolution.com/partner/api/allcustomer_list.php?stv_key="+userInfo.api_key;
        kProgressHUD= Constant.showDialog(this);

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    kProgressHUD.dismiss();
                    try {
                        JSONObject jsonObject=new JSONObject(response);
                        UserInfo userInfo=null;
                        if (jsonObject.getString("code").equals("1")){

                            Toast.makeText(AllCustomersActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();
                            JSONArray jsonArray = jsonObject.getJSONArray("user");
                            allCustomerItemsList=new ArrayList<>();

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                AllCustomerItems allCustomerItems=new AllCustomerItems();
                                allCustomerItems.customer_name=jsonObject1.getString("customer");
                                allCustomerItems.customer_user_key=jsonObject1.getString("user_key");
                                allCustomerItems.customer_id=jsonObject1.getString("id");
                                allCustomerItems.date=jsonObject1.getString("date");
                                allCustomerItems.address=jsonObject1.getString("address");
                                allCustomerItemsList.add(allCustomerItems);
                            }

                        }
                       else if (jsonObject.getString("code").equals("0")){
                            Toast.makeText(AllCustomersActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();

                        }

                        if (allCustomerItemsList.size()>0)
                        {
                            recycle_all_customer.setVisibility(View.VISIBLE);
                            no_result_txt.setVisibility(View.GONE);
                            setAdaptor();
                        }
                        else{
                            recycle_all_customer.setVisibility(View.GONE);
                            no_result_txt.setVisibility(View.VISIBLE);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                kProgressHUD.dismiss();
                String message= Constant.checkConnectionError(error);
                Toast.makeText(AllCustomersActivity.this, message, Toast.LENGTH_SHORT).show();

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));


    }
    public void setAdaptor(){
        allCustomerAdaptor=new AllCustomerAdaptor(this, allCustomerItemsList, new AllCustomerAdaptor.Clicked() {
            @Override
            public void clicked(int pos, View view) {

            }
        });
        recycle_all_customer.setAdapter(allCustomerAdaptor);
    }

}
