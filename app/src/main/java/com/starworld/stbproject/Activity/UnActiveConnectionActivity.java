package com.starworld.stbproject.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.starworld.stbproject.Adaptors.ActiveConnectionAdaptor;
import com.starworld.stbproject.Adaptors.UnActiveConnectionAdaptor;
import com.starworld.stbproject.Items.ActiveConnectionItem;
import com.starworld.stbproject.Items.UnActiveConnectionItem;
import com.starworld.stbproject.Items.UserInfo;
import com.starworld.stbproject.R;
import com.starworld.stbproject.Utils.Constant;
import com.starworld.stbproject.Utils.DroidPrefs;
import com.starworld.stbproject.Volley.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UnActiveConnectionActivity extends AppCompatActivity {


    @BindView(R.id.toolbarunActivec)
    Toolbar toolbarActive;
    @BindView(R.id.recycle_unactive_connection)
    RecyclerView recycle_unactive_connection;
    @BindView(R.id.no_result_txt)
    TextView no_result_txt;
    private LinearLayoutManager linearLayoutManager;
    List<UnActiveConnectionItem> unActiveConnectionItemList;
    UnActiveConnectionAdaptor unActiveConnectionAdaptor;
    private KProgressHUD kProgressHUD;
    UserInfo userInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_un_active_connection);
        ButterKnife.bind(this);

        setSupportActionBar(toolbarActive);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("UnActive Connection");

        userInfo= DroidPrefs.get(this,"user_info",UserInfo.class);

        linearLayoutManager=new LinearLayoutManager(this);
        recycle_unactive_connection.setLayoutManager(linearLayoutManager);
        getUnActiveConnection();



    }


    public void getUnActiveConnection(){
        String url= "http://mydigisolution.com/partner/stvjson/deactive-customrs.php?api_key="+userInfo.api_key+"&dis_key="+userInfo.dis_key;
        kProgressHUD= Constant.showDialog(this);

        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    kProgressHUD.dismiss();
                    try {
                        JSONObject jsonObject=new JSONObject(response);
                        UserInfo userInfo=null;
                        unActiveConnectionItemList=new ArrayList<>();
                        if (jsonObject.getString("responseCode").equals("1")){

                            Toast.makeText(UnActiveConnectionActivity.this,jsonObject.getString("message") , Toast.LENGTH_SHORT).show();
                            JSONArray jsonArray = jsonObject.getJSONArray("responseUserinfo");


                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                UnActiveConnectionItem unActiveConnectionItem=new UnActiveConnectionItem();
                                /*activeConnectionItem.customer_name=jsonObject1.getString("customer_name");
                                activeConnectionItem.mobileno=jsonObject1.getString("mobileno");
                                activeConnectionItem.connectiono=jsonObject1.getString("connectiono");
                                activeConnectionItem.city=jsonObject1.getString("city");
                                activeConnectionItem.address=jsonObject1.getString("address");*/
                                unActiveConnectionItemList.add(unActiveConnectionItem);
                            }

                        }
                        if (jsonObject.getString("responseCode").equals("0")){
                            Toast.makeText(UnActiveConnectionActivity.this,jsonObject.getString("responseMessage") , Toast.LENGTH_SHORT).show();

                        }

                        if (unActiveConnectionItemList.size()>0)
                        {
                            recycle_unactive_connection.setVisibility(View.VISIBLE);
                            no_result_txt.setVisibility(View.GONE);
                            setAdaptor();
                        }
                        else{
                            recycle_unactive_connection.setVisibility(View.GONE);
                            no_result_txt.setVisibility(View.VISIBLE);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                kProgressHUD.dismiss();
                String message= Constant.checkConnectionError(error);
                Toast.makeText(UnActiveConnectionActivity.this, message, Toast.LENGTH_SHORT).show();

            }
        });
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.
                DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 30000));


    }

    public void setAdaptor(){
        unActiveConnectionAdaptor=new UnActiveConnectionAdaptor(this, unActiveConnectionItemList, new UnActiveConnectionAdaptor.Clicked() {
            @Override
            public void clicked(int pos, View view) {

            }
        });
        recycle_unactive_connection.setAdapter(unActiveConnectionAdaptor);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()) {
            case android.R.id.home:
                super. onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
