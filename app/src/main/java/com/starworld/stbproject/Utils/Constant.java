package com.starworld.stbproject.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by yugasalabs-45 on 29/8/17.
 */

public class Constant {
    public static int SNACKBAR_DURATION=2000;
    public static String driver_location_reciever = "driverlocation.service.receiver";
    public static String admin_location_reciever = "adminlocation.service.receiver";
    public static String parent_location_reciever = "parentlocation.service.receiver";
    public static String driver_send_location_reciever = "driver_send_location.service.receiver";
    public static boolean isInternetConnected(Context mContext) {

        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public static String checkConnectionError(VolleyError volleyError){
        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }
        else{
            try {
                if(volleyError.networkResponse!=null) {
                    if (volleyError.networkResponse.data != null) {
                        String responseBody = new String(volleyError.networkResponse.data, "utf-8");
                        JSONObject exception = new JSONObject(responseBody);
                        message= exception.getString("message");
                    }
                }


            } catch ( JSONException e ) {
                e.printStackTrace();
//Handle a malformed json response
            } catch (UnsupportedEncodingException e){
                e.printStackTrace();
            }
        }

       return message;
    }

    public static KProgressHUD showDialog(Context context){
        KProgressHUD  kProgressHUD= KProgressHUD.create(context);
        kProgressHUD .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        kProgressHUD  .setCancellable(false);
        kProgressHUD .setAnimationSpeed(2);
        kProgressHUD .setDimAmount(0.5f);
        kProgressHUD .show();
        return kProgressHUD;
    }
}
