package com.starworld.stbproject.Adaptors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.starworld.stbproject.Items.ActiveConnectionItem;
import com.starworld.stbproject.Items.AllPlanItems;
import com.starworld.stbproject.R;

import java.util.List;

/**
 * Created by 91783 on 20-05-2018.
 */

public class AllPlanDataAdaptor extends RecyclerView.Adapter<AllPlanDataAdaptor.MyViewholder> {

    private Context context;
    private List<AllPlanItems> allPlanItemsList;
    private Clicked clicked;
    public AllPlanDataAdaptor(Context context,List<AllPlanItems> allPlanItemsList,Clicked clicked){
        this.context=context;
        this.allPlanItemsList=allPlanItemsList;
        this.clicked=clicked;
    }
    public interface Clicked{
        public void clicked(int pos, View view);
    }
    @NonNull
    @Override
    public AllPlanDataAdaptor.MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context)
                .inflate(R.layout.activity_all_plan_data_adapter, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AllPlanDataAdaptor.MyViewholder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return allPlanItemsList.size();
    }

    public class MyViewholder extends RecyclerView.ViewHolder {

        public MyViewholder(View itemView) {
            super(itemView);
        }
    }
}
