package com.starworld.stbproject.Adaptors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.starworld.stbproject.Items.CollectedAmountItem;
import com.starworld.stbproject.Items.TotalCollectionItem;
import com.starworld.stbproject.R;

import java.util.List;

/**
 * Created by 91783 on 20-05-2018.
 */

public class CollectedAmountAdaptor extends RecyclerView.Adapter<CollectedAmountAdaptor.MyViewholder> {

    private Context context;
    private List<CollectedAmountItem> collectedAmountItemList;
    private Clicked clicked;
    public CollectedAmountAdaptor(Context context,List<CollectedAmountItem> collectedAmountItemList, Clicked clicked){
        this.context=context;
        this.collectedAmountItemList=collectedAmountItemList;
        this.clicked=clicked;
    }
    public interface Clicked{
        public void clicked(int pos, View view);
    }
    @NonNull
    @Override
    public CollectedAmountAdaptor.MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context)
                .inflate(R.layout.activity_all_plan_data_adapter, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CollectedAmountAdaptor.MyViewholder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return collectedAmountItemList.size();
    }

    public class MyViewholder extends RecyclerView.ViewHolder {

        public MyViewholder(View itemView) {
            super(itemView);
        }
    }
}
