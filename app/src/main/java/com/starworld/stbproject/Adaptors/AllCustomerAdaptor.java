package com.starworld.stbproject.Adaptors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.starworld.stbproject.Items.AllCustomerItems;
import com.starworld.stbproject.Items.AllPlanItems;
import com.starworld.stbproject.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 91783 on 20-05-2018.
 */

public class AllCustomerAdaptor extends RecyclerView.Adapter<AllCustomerAdaptor.MyViewholder> {

    private Context context;
    private List<AllCustomerItems> allCustomerItemsList;
    private Clicked clicked;
    public AllCustomerAdaptor(Context context, List<AllCustomerItems> allCustomerItemsList, Clicked clicked){
        this.context=context;
        this.allCustomerItemsList=allCustomerItemsList;
        this.clicked=clicked;
    }
    public interface Clicked{
        public void clicked(int pos, View view);
    }
    @NonNull
    @Override
    public AllCustomerAdaptor.MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context)
                .inflate(R.layout.all_customer_item, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AllCustomerAdaptor.MyViewholder holder, int position) {
        holder.Cus_name.setText(allCustomerItemsList.get(position).customer_name);
        holder.pplaceIcon.setText(allCustomerItemsList.get(position).customer_name.substring(0,1).toUpperCase());
        holder.date.setText(allCustomerItemsList.get(position).date);
        holder.address.setText(allCustomerItemsList.get(position).address);

    }

    @Override
    public int getItemCount() {
        return allCustomerItemsList.size();
    }

    public class MyViewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.pplaceIcon)
        TextView pplaceIcon;
        @BindView(R.id.cus_name)
        TextView Cus_name;
        @BindView(R.id.txt_date)
        TextView date;
        @BindView(R.id.txt_unique_id)
        TextView unique_id;
        @BindView(R.id.txt_prev_bal)
        TextView prev_bal;
        @BindView(R.id.txt_address)
        TextView address;
        @BindView(R.id.txt_total_amount)
        TextView total_amount;


        public MyViewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
