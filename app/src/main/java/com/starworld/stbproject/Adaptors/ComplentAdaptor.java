package com.starworld.stbproject.Adaptors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.starworld.stbproject.Items.AllPlanItems;
import com.starworld.stbproject.Items.ComplentItem;
import com.starworld.stbproject.R;

import java.util.List;

/**
 * Created by 91783 on 20-05-2018.
 */

public class ComplentAdaptor extends RecyclerView.Adapter<ComplentAdaptor.MyViewholder> {

    private Context context;
    private List<ComplentItem> ComplentItem;
    private Clicked clicked;
    public ComplentAdaptor(Context context, List<ComplentItem> ComplentItem, Clicked clicked){
        this.context=context;
        this.ComplentItem=ComplentItem;
        this.clicked=clicked;
    }
    public interface Clicked{
        public void clicked(int pos, View view);
    }
    @NonNull
    @Override
    public ComplentAdaptor.MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context)
                .inflate(R.layout.complent_item, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ComplentAdaptor.MyViewholder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return ComplentItem.size();
    }

    public class MyViewholder extends RecyclerView.ViewHolder {

        public MyViewholder(View itemView) {
            super(itemView);
        }
    }
}
