package com.starworld.stbproject.Adaptors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.starworld.stbproject.Items.AllPlanItems;
import com.starworld.stbproject.Items.UnActiveConnectionItem;
import com.starworld.stbproject.R;

import java.util.List;

/**
 * Created by 91783 on 20-05-2018.
 */

public class UnActiveConnectionAdaptor extends RecyclerView.Adapter<UnActiveConnectionAdaptor.MyViewholder> {

    private Context context;
    private List<UnActiveConnectionItem> unActiveConnectionItems;
    private Clicked clicked;
    public UnActiveConnectionAdaptor(Context context, List<UnActiveConnectionItem> unActiveConnectionItems, Clicked clicked){
        this.context=context;
        this.unActiveConnectionItems=unActiveConnectionItems;
        this.clicked=clicked;
    }
    public interface Clicked{
        public void clicked(int pos, View view);
    }
    @NonNull
    @Override
    public UnActiveConnectionAdaptor.MyViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context)
                .inflate(R.layout.activity_un_active_connection_adapter, parent, false);
        return new MyViewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UnActiveConnectionAdaptor.MyViewholder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return unActiveConnectionItems.size();
    }

    public class MyViewholder extends RecyclerView.ViewHolder {

        public MyViewholder(View itemView) {
            super(itemView);
        }
    }
}
