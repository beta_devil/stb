package com.starworld.stbproject.Adaptors;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.starworld.stbproject.Activity.AddDataActivity;
import com.starworld.stbproject.Items.ActiveConnectionItem;
import com.starworld.stbproject.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 91783 on 20-05-2018.
 */

public class ActiveConnectionAdaptor extends RecyclerView.Adapter<ActiveConnectionAdaptor.MyViewHolder> {

    private Context context;
    private List<ActiveConnectionItem> activeConnectionItemList;
    private Clicked clicked;
    public ActiveConnectionAdaptor(Context context, List<ActiveConnectionItem> activeConnectionItemList,Clicked clicked){
       this.context=context;
       this.activeConnectionItemList=activeConnectionItemList;
       this.clicked=clicked;
    }
    public interface Clicked{
        public void clicked(int pos, View view);
    }
    @NonNull
    @Override
    public ActiveConnectionAdaptor.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context)
                .inflate(R.layout.activity_activate_connection_adapter, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ActiveConnectionAdaptor.MyViewHolder holder, int position) {
        holder.Cname.setText(activeConnectionItemList.get(position).customer_name);
        holder.connection.setText(activeConnectionItemList.get(position).connectiono);
        holder.mobileno.setText(activeConnectionItemList.get(position).mobileno);
        holder.city.setText(activeConnectionItemList.get(position).city);
        holder.address.setText(activeConnectionItemList.get(position).address);
        holder.UnActiveConnection.setTag(position);
        holder.viewPLan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, AddDataActivity.class);
                context.startActivity(intent);
            }
        });

        holder.UnActiveConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(context, "Working", Toast.LENGTH_SHORT).show();
                int pos= (int) v.getTag();
                if(clicked!=null){
                    clicked.clicked(pos,v);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return activeConnectionItemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_name)
        TextView Cname;
        @BindView(R.id.txt_connection)
        TextView connection;
        @BindView(R.id.txt_contect)
        TextView mobileno;
        @BindView(R.id.txt_city)
        TextView city;
        @BindView(R.id.txt_address)
        TextView address;
        @BindView(R.id.btn_UnActiveConnection)
        Button UnActiveConnection;
        @BindView(R.id.btn_ViewPlan)
        Button viewPLan;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }
}
