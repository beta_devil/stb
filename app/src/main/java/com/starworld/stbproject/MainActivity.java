package com.starworld.stbproject;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.starworld.stbproject.Activity.ActiveConectionActivity;
import com.starworld.stbproject.Activity.AddDataActivity;
import com.starworld.stbproject.Activity.AllCustomersActivity;
import com.starworld.stbproject.Activity.CollectedAmountCustomerListActivity;
import com.starworld.stbproject.Activity.ComplentActivity;
import com.starworld.stbproject.Activity.CustomerDetailsActivity;
import com.starworld.stbproject.Activity.LoginActivity;
import com.starworld.stbproject.Activity.MapsActivity;
import com.starworld.stbproject.Activity.OutStandingAmountCustomerListActivity;
import com.starworld.stbproject.Activity.PaymentActivity;
import com.starworld.stbproject.Activity.PaymentHistoryActivity;
import com.starworld.stbproject.Activity.ProfileActivity;
import com.starworld.stbproject.Activity.TotalCollectionCustomerListActivity;
import com.starworld.stbproject.Activity.UnActiveConnectionActivity;
import com.starworld.stbproject.Activity.UploadActivity;
import com.starworld.stbproject.Items.UserInfo;
import com.starworld.stbproject.Utils.DroidPrefs;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {


    Button btn_payment;
    @BindView(R.id.totalCollectionL1)
    LinearLayout TotalCollectionL1;
    @BindView(R.id.collectedAmountL2)
    LinearLayout CollectedAmountL2;
    @BindView(R.id.customersL5)
    LinearLayout customersL5;
    @BindView(R.id.outStandingAmountL3)
    LinearLayout OutStandingAmountL3;
    @BindView(R.id.lcomplent)
    LinearLayout lcomplent;
    @BindView(R.id.layoutMap)
    LinearLayout layoutMap;
    @BindView(R.id.txt_ActiveConection)
    TextView ActiveConection;
    @BindView(R.id.txt_UnActiveConnection)
    TextView UnActiveConection;

    TextView text_name;
    TextView text_city;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    UserInfo userInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        text_city=headerView.findViewById(R.id.text_city);
        text_name=headerView.findViewById(R.id.text_name);

        TotalCollectionL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, TotalCollectionCustomerListActivity.class);
                startActivity(intent);
            }
        });

        CollectedAmountL2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, CollectedAmountCustomerListActivity.class);
                startActivity(intent);
            }
        });
        customersL5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, AllCustomersActivity.class);
                startActivity(intent);
            }
        });

        OutStandingAmountL3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, OutStandingAmountCustomerListActivity.class);
                startActivity(intent);
            }
        });

        lcomplent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, ComplentActivity.class);
                startActivity(intent);
            }
        });

        layoutMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(MainActivity.this, MyLocationUsingLocationAPI.class);
                startActivity(intent);*/
            }
        });

        ActiveConection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ActiveConectionActivity.class);
                startActivity(intent);
            }
        });

        UnActiveConection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,UnActiveConnectionActivity.class);
                startActivity(intent);
            }
        });


    }
   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.new_Customer) {

            Intent intent=new Intent(MainActivity.this, UploadActivity.class);
            startActivity(intent);
            // Handle the camera action
        } else if (id == R.id.nav_Payment_History) {
            Intent intent=new Intent(MainActivity.this, PaymentHistoryActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_profile) {
            Intent intent=new Intent(MainActivity.this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_Billing) {
            Intent intent=new Intent(MainActivity.this, PaymentActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_AddData) {
            Intent intent=new Intent(MainActivity.this, AddDataActivity.class);
            startActivity(intent);
        }else if (id == R.id.nav_location) {
            Intent intent=new Intent(MainActivity.this, MapsActivity.class);
            startActivity(intent);
        }else if (id == R.id.nav_CustomerList) {
            Intent intent=new Intent(MainActivity.this, AllCustomersActivity.class);
            startActivity(intent);
        }else if (id == R.id.nav_SingOut) {

            DroidPrefs.getDefaultInstance(this).clear();
            Intent intnt = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intnt);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        userInfo=DroidPrefs.get(this,"user_info",UserInfo.class);
        text_name.setText(userInfo.cb_name);
        text_city.setText(userInfo.area_name);

    }
}
