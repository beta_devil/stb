package com.starworld.stbproject.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.starworld.stbproject.Activity.PayMentDetailsActivity;
import com.starworld.stbproject.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 91783 on 20-05-2018.
 */

public class CustomerAccountDetailsFragment extends Fragment {

    @BindView(R.id.btn_paynow)
    Button PayNow;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View  view=inflater.inflate(R.layout.fragment_customer_account_details, container, false);
        ButterKnife.bind(this,view);
        PayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), PayMentDetailsActivity.class);
                startActivity(intent);
            }
        });

        return view;
    }
}
